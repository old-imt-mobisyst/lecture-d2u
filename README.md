# Distributed Decision Under Uncertainty

These pages serve as support for the lecture **D2-U2** apparing in differents module:

- UV Model Agent Decision - **uv-mad**
- Project - Artificial Intelligence and Optimization - **fa-paio**
- Introduction of RL for Health Science

You will find there a presentation of the used notion, the tutorials and some resources to process them.

@Autor: [Guillaume Lozenguez](mailto:guillaune.lozenguez@imt-lille-douai.fr)

## Notions

Presentation of the notions in this lecture:

- 1.0-Introduction - Decision Under Uncertainty
  * State, Action and Policy
- 2.0-reinforcement - Reinforcement Learning
  * Q-Learning on 421 game.
  * 21-results on 421.
- 3.0-Combinatorial - The Curse of Dimentionality
  * groupping similar states together
  * Dimention reduction - Principal Component Annalisis
  * Clustering - k-means
  * Factoring - Decision Tree.  
- 4.0-Learning Structure
- 5.0-Model-Based Decision Making
  * MDP
  * Factored MDP -> Bayesian Network
  * Learn the model
  * Solving large MDP
- 6.0-Distributed Decision Making

Avec un usage intencif de marp (pluggin VisualCode)

Ou standalone: [marp-cli](https://github.com/marp-team/marp-cli) installé en global.

Exemple d'usage:

```bash
marp --theme style/imt.css --pdf notions/the-curse.md --allow-local-files
```
