# Fiche de jeu (étude)

- Jeu: **MoveIt**

Sur une version fixe (**seed 22**)


## Description d'une situation de jeu :

Décrire l'ensemble minimal de variables d'états :

1. 
2. 
3. 
4. 
5. 
6. 
7. 
8. 

Proposer d'autres variables utiles à des prises de décisions :

1. 
2. 
3. 
4. 
5. 
6. 
7. 
8. 

## Q-Learning basée sur Arbre de Décision.

Est-ce que vous identifier une heuristique d'initialisation des Q-Valeurs ?

--- 

--- 

--- 

--- 

--- 

--- 

--- 

--- 

Proposez un premier arbre de décision 

--- 

--- 

--- 

--- 

--- 

--- 

--- 

--- 

Tentez un apprentissage.
