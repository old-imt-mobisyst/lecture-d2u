# Fiche:  Processus à Contrôler

- Jeu: **MoveIt**

## Les états

Décrire les variables d'états (nom, domaine de variation):

1. 
2. 
3. 
4. 
5. 
6. 
7. 
8. 

En première approximation il y a combien d'états (calcul et résultat): 

--- 

--- 

--- 

--- 

--- 


Est-ce qu'il existe des états non atteignables ou redondants (et est-ce qu'il sont nombreux) ?

--- 

--- 

--- 

--- 

--- 


## Les actions

Décrire les variables d'action (nom, domaine de variation):

1. 
2. 
3. 
4. 
5. 
6. 
7. 
8. 

Il y a combien d'action (dans la pire configuration possible) ?

--- 

--- 

--- 

--- 

--- 

Évaluer le branching (dans la pire configuration possible) ?

--- 

--- 

--- 

--- 

--- 


## Une première IA

En deux trois phrases, décrire une IA purement heuristique.

--- 

--- 

--- 

--- 

--- 


## Optimization: 

Proposer une fonction de réconpences $r(s, a, s')$ pour ce jeu.

--- 

--- 

--- 

--- 

--- 
